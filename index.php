<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>

<body>
  <!-- Header of website -->
  <div class="container">
    <div class="header">
      <h2>My Website</h2>
      <h6>Welcome to my website</h6>
    </div>
    <hr>
  </div>

  <div class="container">
    <div class="all">
      <div class="row">
        <!-- Post of weblog -->
        <div class="col-lg-8 ">
          <div class="content">
            <img src="1.png" alt="programming" class="content-img">
            <div class="mohtava">
              <h3>What is programming</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Laboriosam dolores eaque excepturi odit magnam nam nisi a
                repudiandae sunt, quo, sed obcaecati architecto ullam sint.
                Cumque distinctio eligendi facilis accusantium?
              </p>
              <input type="button" value="read more" class="btn btn-success">
            </div>
            <hr>
          </div>
          <div class="content">
            <div class="mohtava">
              <img src="1.png" alt="programming" class="content-img">
              <h3>What is programming</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Laboriosam dolores eaque excepturi odit magnam nam nisi a
                repudiandae sunt, quo, sed obcaecati architecto ullam sint.
                Cumque distinctio eligendi facilis accusantium?
              </p>
              <input type="button" value="read more" class="btn btn-success">
            </div>
            <hr>
          </div>
        </div>


        <!----------------------------------  Sidebar ---------------------------------->

        <div class="col-lg-3 offset-lg-1 ">
          <div class="move-center">


            <!----------------------------------  About Me ---------------------------------->
            <div class="sidebar">
              <h2 class="jarzan">Advertise</h2>
              <img src="2.png" alt="sidebar" class="sidebar-image">
            </div>
            <div class="under-sidebar">
              <h4>About Me</h4>
              <p class="text1" style="text-align: justify; word-spacing: -3.5px;">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Laboriosam dolores eaque excepturi odit magnam nam nisi a
                repudiandae sunt, quo, sed obcaecati architecto ullam sint.
                Cumque distinctio eligendi facilis accusantium?
              </p>
            </div>
            <!---------------------------------- Properties ---------------------------------->
            <div class="properties">
              <div class="title">
                <h5>Properties</h5>
              </div>
              <div class="products">
                <img src="3.png" alt="" class="be-good">
                <p class="helpme">Version 1</p><br>
                <img src="4.png" alt="" class="be-good">
                <p class="helpme">Version 2</p><br>
                <img src="5.png" alt="" class="be-good">
                <p class="helpme">Version 3</p>
              </div>
            </div>
            <!---------------------------------- Footer ---------------------------------->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <footer class="my-footer">
      <p> Copywrite by Amir Tavakolian :D </p>
    </footer>
  </div>
</body>

</html>